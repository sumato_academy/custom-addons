# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CustomCRM(models.Model):
    _inherit = 'crm.lead'

    crm_line = fields.One2many('res.partner', 'crm_id', string="CRM Line")


class CustomCRMLine(models.Model):
    _inherit = 'res.partner'

    crm_id = fields.Many2one('crm.lead', string="CRM ID")
    dob = fields.Date("Birthday")
