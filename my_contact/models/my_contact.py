# -*- coding: utf-8 -*-

from odoo import models, fields, api

class MyContact(models.Model):

    _name = "my.contact"
    _description = "My Contacts model"

    #Add field
    name = fields.Char('Contact Name', required=True)
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female')
    ], string='Gender', default='male')
    dob = fields.Date('DOB', required=False)
    phone = fields.Char('Phone')
    email = fields.Char('Email')
    address = fields.Char('Address')
