=====================
[1.0.2]
[MOD] Website side "Internal User" access right add.
=====================
[1.0.1]
[MOD] Hours format changed into HH:MM format.
=====================
[1.0]
[Mod] Migrate code from v12.0 to v13.0
