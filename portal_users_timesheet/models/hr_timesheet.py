# -*- coding: utf-8 -*-
# Part of Odoo. See COPYRIGHT & LICENSE files for full copyright and licensing details.

from odoo import api, fields, models, _


class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    def _compute_access_url(self):
        super(AccountAnalyticLine, self)._compute_access_url()
        for timesheet in self:
            timesheet.access_url = '/my/timesheet/%s' % timesheet.id

    def convert_time(self, float_val):
        result = False
        hours, minutes = 0, 0
        if float_val:
            hours, minutes = divmod((float_val * 60), 60)
        return {'hours': int(hours), 'minutes': int(minutes)}
