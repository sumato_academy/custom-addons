# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CustomSaleOrderLine(models.Model):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'
    _description = 'Customize sales'

    student_id = fields.Many2one('res.partner', string="Name", ondelete='set null')
